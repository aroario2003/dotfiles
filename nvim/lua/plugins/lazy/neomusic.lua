return {
    {
        dir = "~/repos/lua/neomusic.nvim",
        config = function()
            require("neomusic").setup({})
        end
    },
}
