return {
    {
        dir = "~/repos/lua/neostatus.nvim",
        config = function()
            require("neostatus").setup()
        end
    },
}
