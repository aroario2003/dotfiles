return {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
        require("nvim-treesitter.configs").setup({
            ensure_installed = { "rust", "lua", "bash", "ocaml", "go", "markdown" },
            sync_install = false,
            highlight = { enable = true },
            indent = { enable = true }
        })
    end
}
