local o = vim.o
local g = vim.g

--vim options--
o.updatetime = 2000
o.number = true
o.relativenumber = true
o.termguicolors = true
o.expandtab = true
o.hidden = true
o.incsearch = true
o.laststatus = 2
o.tabstop = 4
o.hlsearch = false
o.scrolloff = 8
o.shiftwidth = 4
o.ignorecase = true
o.clipboard = unnamedplus
o.ttyfast = true
o.mouse = 'a'
o.showtabline = 2
o.ignorecase = true
o.smartcase = true
o.undofile = true
o.laststatus = 3
g.nocompatible = true

vim.cmd("filetype plugin on")

--set colorscheme--
g.background = dark
vim.cmd([[colorscheme tokyonight-night]])
