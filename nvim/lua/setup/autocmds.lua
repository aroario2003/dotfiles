--yank text into system clipboard as well as neovim one
vim.api.nvim_create_autocmd("TextYankPost", {
    callback = function()
        local content = vim.v.event.regcontents
        vim.fn.setreg("+", content)
    end
})

--Format on buffer save
vim.api.nvim_create_autocmd("BufWritePre", {
    callback = function()
        vim.lsp.buf.format()
    end
})
