(org-babel-load-file
 (expand-file-name
  "config.org"
  user-emacs-directory))


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("8c7e832be864674c220f9a9361c851917a93f921fedb7717b1b5ece47690c098" "aec7b55f2a13307a55517fdf08438863d694550565dee23181d2ebd973ebd6b8" "34cf3305b35e3a8132a0b1bdf2c67623bc2cb05b125f8d7d26bd51fd16d547ec" "da75eceab6bea9298e04ce5b4b07349f8c02da305734f7c0c8c6af7b5eaa9738" default))
 '(package-selected-packages '(slime zig-mode phps-mode haskell-mode php-mode magit)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(hl-line ((t (:height 1.0))))
 '(org-document-title ((t (:height 4.5))))
 '(org-level-1 ((t (:foreground "#7aa2f7" :height 3.5))))
 '(org-level-2 ((t (:foreground "#ad8ee6" :height 3.0))))
 '(org-level-3 ((t (:foreground "#9ece6a" :height 2.5))))
 '(org-level-4 ((t (:foreground "#449dab" :height 2.0))))
 '(org-level-5 ((t (:foreground "#e0af68" :height 1.5))))
 '(org-level-6 ((t (:foreground "#f7768e" :height 1.0)))))
