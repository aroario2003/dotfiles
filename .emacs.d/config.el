(defvar elpaca-installer-version 0.8)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
			      :ref nil
			      :files (:defaults (:exclude "extensions"))
			      :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
	(if-let ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
		 ((zerop (call-process "git" nil buffer t "clone"
				       (plist-get order :repo) repo)))
		 ((zerop (call-process "git" nil buffer t "checkout"
				       (or (plist-get order :ref) "--"))))
		 (emacs (concat invocation-directory invocation-name))
		 ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
				       "--eval" "(byte-recompile-directory \".\" 0 'force)")))
		 ((require 'elpaca))
		 ((elpaca-generate-autoloads "elpaca" repo)))
	    (progn (message "%s" (buffer-string)) (kill-buffer buffer))
	  (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))

(elpaca elpaca-use-package
  (elpaca-use-package-mode)
  (setq elpaca-use-package-by-default t))

(use-package emacs :ensure nil :config (setq ring-bell-function #'ignore))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding t)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-undo-system 'undo-fu)
  (evil-mode))

(use-package undo-fu)

(use-package general
  :config
  (general-evil-setup)

  (general-create-definer legend/leader-keys
      :states '(normal insert visual emacs)
      :keymaps 'override
      :prefix "SPC"
      :global-prefix "M-SPC")

  (legend/leader-keys 
    "." '(dired :wk "File Manager"))

  (legend/leader-keys
    "b" '(:ignore t :wk "Buffer")
    "bb" '(switch-to-buffer :wk "Switch buffer")
    "bk" '(kill-this-buffer :wk "Kill buffer")
    "bn" '(next-buffer :wk "Next buffer")
    "bp" '(previous-buffer :wk "Previous buffer")
    "br" '(revert-buffer :wk "Reload buffer")
    "bi" '(ibuffer :wk "Show ibuffer")) 

  (legend/leader-keys 
    "e" '(:ignore t :wk "Evaluate")
    "ee" '(eval-expression :wk "Evaluate Expression")
    "eb" '(eval-buffer :wk "Evaluate Buffer")
    "ec" '(org-ctrl-c-ctrl-c :wk "Evaluate codeblock in org mode"))

  (legend/leader-keys
    "f" '(:ignore t :wk "File")
    "ff" '(find-file :wk "Find File")
    "fs" '(save-buffer :wk "Save File"))

 (legend/leader-keys 
   "p" '(:ignore t :wk "Projectile")
   "pff" '(projectile--find-file :wk "Projectile Find File")
   "pd" '(project-dired :wk "Start dired in current project"))

 (legend/leader-keys 
   "P" '(:ignore t :wk "Package management")
   "Pi" '(package-install :wk "install a package"))

  (legend/leader-keys 
     "g" '(:ignore t :wk "git commands")
     "gi" '(magit-init :wk "initialize git repository"))

  (legend/leader-keys
    "w" '(:ignore t :wk "Window")
    "wn" '(evil-window-vnew :wk "Window New")
    "wh" '(evil-window-left :wk "Window Focus Left")
    "wj" '(evil-window-down :wk "Window Focus Down")
    "wk" '(evil-window-up :wk "Window Focus Up")
    "wl" '(evil-window-right :wk "Window Focus Right")
    "wH" '(buf-move-left :wk "Window Move Left")
    "wJ" '(buf-move-down :wk "Window Move Down")
    "wK" '(buf-move-up :wk "Window Move Up")
    "wL" '(buf-move-right :wk "Window Move Right")
    "wb" '(split-window-below :wk "Horizontal Split")
    "wc" '(evil-window-delete :wk "Close Window"))

  (legend/leader-keys 
    "W" '(:ignore t :wk "Workspace bindings")
    "Ws" '(persp-switch :wk "Switch workspace")
    "Wbi" '(persp-ibuffer :wk "Perspecitve buffer list")
    "Wn" '(persp-next :wk "Go to next workspace")
    "Wp" '(persp-prev :wk "Go to previous workspace"))

  (legend/leader-keys
    "h" '(:ignore t :wk "Help")
    "hrr" '(reload-init-file :wk "Reload init.el")
    "hv" '(describe-variable :wk "Describe Variable")
    "hf" '(describe-function :wk "Describe Function"))

  (legend/leader-keys 
    "t" '(:ignore t :wk "Toggle")
    "tv" '(vterm-toggle :wk "Toggle Vterm")
    "tn" '(neotree-toggle :wk "Toggle Neotree"))

  (legend/leader-keys 
    "r" '(:ignore t :wk "org-roam")
    "rf" '(org-roam-node-find :wk "Find roam node"))

  (legend/leader-keys
    "m" '(:ignore t :wk "multiple cursors")
    "mc" '(mc/edit-lines :wk "Edit lines with multiple curors")
    "mnc" '(mc/mark-next-like-this :wk "Put cursor at next region")
    "mpc" '(mc/mark-previous-like-this :wk "Put cursor at previous region"))
 )

(defun reload-init-file ()
  (interactive)
  (load-file user-init-file)
  (ignore (elpaca-process-queues)))

(require 'windmove)

(defun buf-move-up ()
  "Swap the current buffer and the buffer above the split.
If there is no split, ie now window above the current one, an
error is signaled."
  (interactive)
  (let* ((other-win (windmove-find-other-window 'up))
	 (buf-this-buf (window-buffer (selected-window))))
    (if (null other-win)
        (error "No window above this one")
      (set-window-buffer (selected-window) (window-buffer other-win))
      (set-window-buffer other-win buf-this-buf)
      (select-window other-win))))

(defun buf-move-down ()
"Swap the current buffer and the buffer under the split.
If there is no split, ie now window under the current one, an
error is signaled."
  (interactive)
  (let* ((other-win (windmove-find-other-window 'down))
	 (buf-this-buf (window-buffer (selected-window))))
    (if (or (null other-win) 
            (string-match "^ \\*Minibuf" (buffer-name (window-buffer other-win))))
        (error "No window under this one")
      (set-window-buffer (selected-window) (window-buffer other-win))
      (set-window-buffer other-win buf-this-buf)
      (select-window other-win))))

(defun buf-move-left ()
"Swap the current buffer and the buffer on the left of the split.
If there is no split, ie now window on the left of the current
one, an error is signaled."
  (interactive)
  (let* ((other-win (windmove-find-other-window 'left))
	 (buf-this-buf (window-buffer (selected-window))))
    (if (null other-win)
        (error "No left split")
      (set-window-buffer (selected-window) (window-buffer other-win))
      (set-window-buffer other-win buf-this-buf)
      (select-window other-win))))

(defun buf-move-right ()
"Swap the current buffer and the buffer on the right of the split.
If there is no split, ie now window on the right of the current
one, an error is signaled."
  (interactive)
  (let* ((other-win (windmove-find-other-window 'right))
	 (buf-this-buf (window-buffer (selected-window))))
    (if (null other-win)
        (error "No right split")
      (set-window-buffer (selected-window) (window-buffer other-win))
      (set-window-buffer other-win buf-this-buf)
      (select-window other-win))))

(menu-bar-mode -1)
(tool-bar-mode -1)
(setq make-backup-files nil)
(scroll-bar-mode -1)
(electric-indent-mode -1)
(global-display-line-numbers-mode 1)
(global-visual-line-mode t)
(electric-pair-mode 1)

(setq inferior-lisp-program "clasp")

(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

(use-package which-key
  :init
    (which-key-mode 1)
  :config
    (setq which-key-side-window-location 'bottom
	which-key-sort-order #'which-key-key-order-alpha
	which-key-sort-uppercase-first nil
	which-key-add-column-padding 1
	which-key-max-display-columns nil
	which-key-min-display-lines 6
	which-key-side-window-slot -10
	which-key-side-window-max-height 0.25
	which-key-idle-delay 0.8
	which-key-max-description-length 25
	which-key-allow-imprecise-window-fit t
	which-key-separator " → " ))

(use-package all-the-icons
  :ensure t 
  :if (display-graphic-p))

(use-package all-the-icons-dired 
  :hook dired-mode . (lambda () (all-the-icons-dired-mode t)))

(use-package counsel 
  :after ivy 
  :config (counsel-mode))

(use-package ivy 
  :custom 
    (setq ivy-use-virtual-buffers t)
    (setq ivy-count-format "(%d/%d)")
    (setq enable-recursive-minibuffers t)
  :config 
    (ivy-mode))

(use-package all-the-icons-ivy-rich 
  :ensure t
  :init (all-the-icons-ivy-rich-mode 1))

(use-package ivy-rich 
  :after ivy 
  :ensure t 
  :init (ivy-rich-mode t)
  :custom 
    (ivy-virtual-abbreviate 'full
      ivy-rich-switch-buffer-align-virtual-buffer t
      ivy-rich-path-style 'abbrev)
  :config 
    (ivy-set-display-transformer 'ivy-switch-buffer
                                 'ivy-rich-switch-buffer-transformer))

(use-package vterm 
  :config 
    (setq shell-file-name "/bin/sh"
      vterm-max-scrollback 5000)) 

(use-package vterm-toggle
  :after vterm
  :config
  (setq vterm-toggle-fullscreen-p nil)
  (setq vterm-toggle-scope 'project)
  (add-to-list 'display-buffer-alist
               '((lambda (buffer-or-name _)
                     (let ((buffer (get-buffer buffer-or-name)))
                       (with-current-buffer buffer
                         (or (equal major-mode 'vterm-mode)
                             (string-prefix-p vterm-buffer-name (buffer-name buffer))))))
                  (display-buffer-reuse-window display-buffer-at-bottom)
                  (reusable-frames . visible)
                  (window-height . 0.3))))

(add-hook 'vterm-mode-hook (lambda() (display-line-numbers-mode -1)))

(add-hook 'eshell-mode-hook (lambda () (display-line-numbers-mode -1)))

(use-package rainbow-mode 
  :hook org-mode prog-mode)

(use-package rainbow-delimiters
  :hook ((emacs-lisp-mode . rainbow-delimiters-mode)
         (clojure-mode . rainbow-delimiters-mode)))

(use-package neotree
  :config (setq neo-smart-open t))

(use-package peep-dired
  :ensure t
  :defer t
  :bind (:map dired-mode-map 
              ("P" . peep-dired)))

(require 'dired)
(define-key dired-mode-map (kbd "C-j") 'peep-dired-next-file)
(define-key dired-mode-map (kbd "C-k") 'peep-dired-prev-file)

(add-hook 'dired-mode-hook (lambda () (display-line-numbers-mode -1)))

(use-package projectile
  :ensure t
  :init
  (projectile-mode +1)
  :bind (:map projectile-mode-map
	      ("s-p" . projectile-command-map)
	      ("C-c p" . projectile-command-map)))
(setq projectile-project-root-files-bottom-up '(".git" ".projectile"))

(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))
  (setq dashboard-banner-logo-title "Emacs is not an editor but an elisp interpreter")

(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))

(add-hook 'window-buffer-change-functions (lambda (_win) 
					    (if (string-equal (format "%s" (current-buffer)) "*dashboard*") 
                                               (progn
						 (message "it works")
						 (dashboard-mode)))))

(use-package perspective
  :bind
  ("C-x C-b" . persp-list-buffers)         
  :custom
  (persp-mode-prefix-key (kbd "C-c M-p"))
  :init
  (persp-mode))

(use-package caml)
(use-package tuareg)
(use-package rust-mode)
(use-package haskell-mode)
(use-package zig-mode)
(use-package go-mode)

(use-package evil-nerd-commenter
  :init
  (evilnc-default-hotkeys nil t))

(use-package lsp-java)
(use-package lsp-mode
  :init
  (setq lsp-keymap-prefix "C-c l")
  (setq lsp-ui-sideline-show-diagnostics t)
  :hook (
	 (caml-mode . lsp)
	 (rust-mode . lsp)
	 (go-mode . lsp)
	 (java-mode . lsp)
	 (tuareg-mode . lsp)
	 (python-mode . lsp)
	 (zig-mode . lsp)
	 (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp)
(add-to-list 'exec-path "/home/legend/.opam/default/bin")
(add-to-list 'exec-path "/home/legend/.local/bin")
(add-to-list 'exec-path "/home/legend/go/bin")

(use-package lsp-ui :commands lsp-ui-mode)

(use-package corfu
  :custom
    (corfu-cycle t)                
    (corfu-auto t)                 
    (corfu-auto-delay 0)
  :init
    (global-corfu-mode))

(use-package org-superstar)
(use-package toc-org)
(add-hook 'org-mode-hook (lambda () 
                           (progn 
                             (org-superstar-mode 1) 
                               (toc-org-mode 1))))

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "/home/legend/Documents/org/org-roam/"))
  :config
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode))

(setq org-agenda-files "~/Documents/org/agenda.org")

(add-hook 'org-mode-hook (lambda ()
           (setq-local electric-pair-inhibit-predicate
                   `(lambda (c)
                  (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))))

(use-package transient)
(use-package magit :after transient)

(use-package pdf-tools
    :defer t
    :commands pdf-loader-install
    :mode "\\.pdf\\'"
    :bind (:map pdf-view-mode-map
		("j" . pdf-view-next-line-or-next-page)
		("k" . pdf-view-previous-line-or-previous-page)
		("C-=" . pdf-view-enlarge)
		("c--" . pdf-view-shrink))
    :init (pdf-loader-install)
    :config (add-to-list 'revert-without-query ".pdf"))
(add-hook 'pdf-view-mode-hook (lambda () (interactive) (display-line-numbers-mode -1)))

(use-package mastodon
  :ensure t)

(setq mastodon-instance-url "https://fosstodon.org"
      mastodon-active-user "thelegendlinux")

(use-package elfeed)

(setq elfeed-feeds 
      '(("https://reddit.com/r/linux.rss" reddit linux)
        ("https://reddit.com/r/emacs.rss" reddit emacs)
        ("https://reddit.com/r/archlinux.rss" reddit archlinux)
        ("https://www.phoronix.com/rss.php" phoronix linux)
        ("https://reddit.com/r/linuxmasterrace.rss" linux reddit)
        ("https://reddit.com/r/Pixel7Pro.rss" reddit phone pixel)))

(require 'org-tempo)

(use-package multiple-cursors)

(custom-set-faces
'(hl-line ((t (:height 1.0))))
 '(org-document-title ((t (:height 4.5))))
 '(org-level-1 ((t (:foreground "#7aa2f7" :height 3.5))))
 '(org-level-2 ((t (:foreground "#ad8ee6" :height 3.0))))
 '(org-level-3 ((t (:foreground "#9ece6a" :height 2.5))))
 '(org-level-4 ((t (:foreground "#449dab" :height 2.0))))
 '(org-level-5 ((t (:foreground "#e0af68" :height 1.5))))
 '(org-level-6 ((t (:foreground "#f7768e" :height 1.0)))))

(use-package move-text)

(add-hook 'after-save-hook (lambda () 
			       (if (string-equal (format "%s" major-mode) "org-mode") 
                                 (progn
				   (toc-org-insert-toc) 
				   (org-babel-tangle)))))

(require 'ibuffer)
(define-key ibuffer-mode-map (kbd "j") 'ibuffer-forward-line)
(define-key ibuffer-mode-map (kbd "k") 'ibuffer-backward-line)

(with-eval-after-load 'evil-maps 
(define-key evil-motion-state-map (kbd "SPC") nil)
(define-key evil-motion-state-map (kbd "RET") nil)
(define-key evil-motion-state-map (kbd "TAB") nil))
(setq org-return-follows-link t)

(setq initial-major-mode 'org-mode)

(setq org-confirm-babel-evaluate nil)

(global-set-key [escape] 'keyboard-escape-quit)

(defun simple-mode-line-render (left right)
  "Return a string of `window-width' length containing LEFT, and RIGHT
 aligned respectively."
  (let* ((available-width (- (window-width) (- (length left) 3))))
    (format (format "%%s %%%ds " available-width) left right)))

(defun format-vc-mode () 
  (if (eq vc-mode 'nil) (format "") (format " %s" (substring vc-mode 5 11))))

(defun set-font-faces ()
  (set-face-attribute 'default nil
    :font "Iosevka Nerd Font"
    :height 115
    :weight 'medium)
  (set-face-attribute 'variable-pitch nil
    :font "Mononoki Nerd Font"
    :height 120
    :weight 'medium)
  (set-face-attribute 'fixed-pitch nil
    :font "Iosevka nerd Font"
    :height 11
    :weight 'medium)
  (set-face-attribute 'font-lock-comment-face nil
    :slant 'italic)
  (set-face-attribute 'font-lock-keyword-face nil
    :slant 'italic))

(setq prettify-symbols-alist
  '(
     ("lambda" . 955)
   ))
(global-prettify-symbols-mode 1)

(use-package doom-themes
  :ensure t
  :config
  (setq doom-theme-enable-bold t
	doom-themes-enable-italic t)
(load-theme 'doom-tokyo-night t))

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

;; (set-face-background 'mode-line "#1a1b26")
;; (set-face-foreground 'mode-line "#a9b1d6")
;; (set-face-background 'mode-line-inactive "#14151e")
;; (set-face-foreground 'mode-line-inactive "#a9b1d6")
;; (set-face-attribute 'mode-line nil :underline "#30313b") 
;; (set-face-attribute 'mode-line nil :overline "#30313b") 
;; (set-face-attribute 'mode-line-inactive nil :box nil)

;; (setq-default mode-line-format 
;;   '(:eval (simple-mode-line-render
;;     (concat
;; 	(propertize "▊ " 'face '(:background "#1a1b26" :foreground "#7aa2f7"))
;; 	(cond ((eq evil-state 'normal) (propertize "●" 'face '(:background "#1a1b26" :foreground "#9ece6a")))
;; 	(t (propertize "●" 'face '(:background "#1a1b26" :foreground "#f7768e"))))
;; 	" " 
;; 	(cond ((buffer-modified-p) (propertize (format "%s" (buffer-name)) 'face '(:background "#1a1b26" :foreground "#f7768e")))
;; 	(t (propertize (format "%s" (buffer-name) 'face '(:background "#1a1b26" :foreground "white")))))
;; 	" "
;; 	(propertize (format-mode-line "%l"))
;; 	":"
;; 	(propertize (format-mode-line "%c"))
;; 	" "
;; 	(propertize (format-mode-line mode-line-percent-position))
;; 	"% " 
;; 	)
;; 	(concat 
;;       (propertize (capitalize (string-replace "-mode" "" (symbol-name major-mode))))
;; 	" "
;; 	(propertize (format-vc-mode))
;; 	" "
;; 	))))
